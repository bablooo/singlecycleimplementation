module controlsignalgenerator(opcode,j,br,memtoreg,memwrite,memread,alusrc,regdes,regwrite,aluop);
 input[5:0] opcode;
output br,memtoreg,memwrite,memread,j,alusrc,regdes,regwrite;
 output[1:0] aluop;
 reg br,memtoreg,memwrite,memread,j,alusrc,regdes,regwrite;
 reg[1:0] aluop;
 always@(opcode)
  begin
  case(opcode)
    6'd0: 
      begin
      j=0;
      br=0;
      memtoreg=0;
      memwrite=0;
      memread=0;
      alusrc=0;
      regdes=1;
      regwrite=1;
      aluop=2'b00;
      end
    6'd1: 
      begin
      j=0;
      br=0;
      memtoreg=0;
      memwrite=0;
      memread=0;
      alusrc=1;
      regdes=0;
      regwrite=1;
      aluop=2'b11;
      end
    6'd2: 
      begin
      j=0;
      br=0;
      memtoreg=1;
      memwrite=0;
      memread=1;
      alusrc=1;
      regdes=0;
      regwrite=1;
      aluop=2'b11;
      end
    6'd3: 
      begin
      j=0;
      br=0;
      memtoreg=0;
      memwrite=1;
      memread=0;
      alusrc=1;
      regdes=0;
      regwrite=0;
      aluop=2'b11;
      end
    6'd4: 
      begin
      j=0;
      br=1;
      memtoreg=0;
      memwrite=0;
      memread=0;
      alusrc=0;
      regdes=0;
      regwrite=0;
      aluop=2'b01;
      end
    6'd5: 
      begin
      j=1;
      br=0;
      memtoreg=0;
      memwrite=0;
      memread=0;
      alusrc=0;
      regdes=0;
      regwrite=0;
      aluop=2'b00;
      end
    6'd6: 
      begin
      j=0;
      br=0;
      memtoreg=0;
      memwrite=0;
      memread=0;
      alusrc=1;
      regdes=0;
      regwrite=1;
      aluop=2'b10;
      end
 endcase
end
endmodule















module alucontrolunit(aluop,func,alucontrol);
  input [1:0] aluop;
  input [5:0] func;
  output [3:0] alucontrol;
  reg [3:0] alucontrol;
always@(aluop or func)
begin 
  case(aluop)
       2'd0:
           begin 
               case(func)
                  6'd0:
                    begin
                    alucontrol=4'd0;
                     end
                  6'd1:
                    begin
                    alucontrol=4'd1;
                     end
                  6'd2:
                    begin
                    alucontrol=4'd2;
                     end
                  6'd3:
                    begin
                    alucontrol=4'd3;
                     end
                   6'd4:
                    begin
                    alucontrol=4'd4;
                     end
endcase
end
2'd3:
begin
alucontrol=4'd0;
end
2'd1:
begin
alucontrol=4'd1;
end
2'd2:
begin
alucontrol=4'd4;
end

endcase
end
endmodule












module SignExtend(in,out);
input [15:0] in;
output [31:0] out;
assign out[31:16] = 1'b0;
assign out[15:0] = in[15:0];
endmodule












module Shift2bit(in,out);
  input [31:0]in;
  output [31:0]out;
  reg [31:0]out;
  always@(in)
 begin      
out=in<<2;
  end
endmodule










module alu(A,B,alucontrol,out,zerosig);
input [31:0] A,B;
output [31:0] out;
output zerosig;
input[3:0] alucontrol;
reg [31:0] out;
always@(A or B or alucontrol)
begin
case(alucontrol)
4'd0:
begin
out=A+B;
end
4'd1:
begin
out=A-B;
end
4'd2:
begin
out=A&B;
end
4'd3:
begin
out=A|B;
end
4'd4:
begin
out=(A<B)? 32'd1:32'd0;
end
endcase
end
assign zerosig=(out==32'd0)?1'b1:1'b0;
endmodule








module PCincrem(pc,pcplus4);
input [31:0] pc;
output [31:0] pcplus4;
assign pcplus4=pc+32'd4;
endmodule








module instr_mem(clk,pc,instruction );  
input clk;
input [31:0] pc; 
output [31:0]  instruction;
reg [31:0] instruction;
 reg [7:0] instrmem[15:0];  
      initial  
      begin  
instrmem[0] = 8'h00; 
instrmem[1] = 8'h41;   
instrmem[2] = 8'h18;
instrmem[3] =8'h00;
instrmem[4] =8'h00;
instrmem[5] = 8'h64;
instrmem[6] = 8'h28;
instrmem[7] =   8'h01;
instrmem[8] =  8'h00;
instrmem[9] = 8'ha3;
instrmem[10] =   8'h30;
instrmem[11] =   8'h04;
instrmem[12] =  8'h09;
instrmem[13] =  8'h07;
instrmem[14] = 8'h00;
instrmem[15] = 8'h04;
                   
      end  
    always@(clk)  
  begin
 instruction[31:24]=instrmem[pc[3:0]];
  instruction[23:16]=instrmem[pc[3:0]+1];
  instruction[15:8]=instrmem[pc[3:0]+2];
  instruction[7:0]=instrmem[pc[3:0]+3];

end
 endmodule   

                 






module register_file(rst,reg_write_en,reg_write_dest,reg_write_data,reg_read_addr_1,reg_read_addr_2, reg_read_data_1,reg_read_data_2);  
input rst,reg_write_en;
 input [4:0] reg_write_dest,reg_read_addr_1, reg_read_addr_2; 
 input [31:0] reg_write_data;
output [31:0] reg_read_data_1, reg_read_data_2;
reg [31:0] reg_read_data_1, reg_read_data_2; 
 reg [31:0] reg_file[31:0];  
always @ (posedge rst or reg_write_data or reg_read_addr_1 or reg_read_addr_2) begin  
if(rst) begin  
reg_file[0] = 32'd1;  
reg_file[1] = 32'd5;  
reg_file[2] = 32'd15;  
reg_file[3] = 32'd0;  
 reg_file[4] = 32'd5;  
  reg_file[5] = 32'd0;  
 reg_file[6] = 32'd0;  
reg_file[7] = 32'd0;     
reg_file[8] = 32'd10;  
reg_file[9] = 32'd2;  
 reg_file[10]= 32'd5;  
 reg_file[11] = 32'd5;  
reg_file[12] = 32'd0;  
reg_file[13] = 32'd10;  
reg_file[14] = 32'd56;  
reg_file[15] = 32'd8;     
reg_file[16] = 32'd1;  
 reg_file[17] = 32'd2;  
reg_file[18] = 32'd5;  
reg_file[19] = 32'd5;  
reg_file[20] = 32'd0;  
reg_file[21] = 32'd10;  
reg_file[22] = 32'd56;  
reg_file[23] = 32'd8;  
reg_file[24] = 32'd8;     
reg_file[25] = 32'd1;  
reg_file[26] = 32'd2;  
reg_file[27] = 32'd5;  
 reg_file[28] = 32'd5;  
 reg_file[29] = 32'd0;  
reg_file[30] = 32'd10;  
reg_file[31] = 32'd56; 
reg_read_data_1 = reg_file[reg_read_addr_1];  
reg_read_data_2 = reg_file[reg_read_addr_2];   
end  
 else if(reg_write_en) begin  
 reg_read_data_1 = reg_file[reg_read_addr_1];  
 reg_read_data_2 = reg_file[reg_read_addr_2];  
   reg_file[reg_write_dest] <= reg_write_data;  
 end  
else 
begin
reg_read_data_1 = reg_file[reg_read_addr_1];  
reg_read_data_2 = reg_file[reg_read_addr_2];  
end end
 endmodule   










 module data_memory (clk,mem_access_addr,mem_write_data,mem_write,mem_read, mem_read_data);  
 input clk;  
input [31:0] mem_access_addr;
input [31:0] mem_write_data; 
input mem_write;  
 input mem_read; 
output [31:0] mem_read_data;  
integer i;  
reg [31:0] datamem [255:0];  
wire [31 : 0] datamem_addr = {24'd0,mem_access_addr[7: 0]};  
 initial begin  
 for(i=0;i<256;i=i+1)  
 datamem[i] <= i;  
      end  
      always @(posedge clk) begin  
if (mem_write)  
datamem[datamem_addr] <= mem_write_data;  
      end  
      assign mem_read_data = (mem_read==1'b1) ? datamem[datamem_addr]: 32'd0;   
 endmodule 










module mips(clk,rst,aluout,pcout);
input rst,clk;
output [31:0] aluout,pcout;
reg [31:0] current_pc;
wire[31:0] next_pc,instruction,pcplus4,rega,regb,regrr,regimm,memreaddata,writedata,brjoffset,pcbreak,pcj;
wire[4:0] regw;
wire br,memtoreg,memwrite,memread,j,alusrc,regdes,regwrite,zerosig,beq;
wire[1:0] aluop;
wire[3:0] alucontrol;
always @ ( posedge clk or posedge rst)
 begin  
  if(rst==1'b1)  
   begin 
    current_pc<=32'd0;
   end
 else
   begin 
    current_pc<=next_pc;
   end
end
PCincrem pcincre(current_pc,pcplus4);
instr_mem instr(clk,current_pc,instruction);
controlsignalgenerator ff(instruction[31:26],j,br,memtoreg,memwrite,memread,alusrc,regdes,regwrite,aluop);
assign regw = (regdes==1'b1)? instruction[15:11]:instruction[20:16];
register_file regg(rst,regwrite,regw,writedata,instruction[25:21],instruction[20:16], rega,regrr);  
SignExtend ss(instruction[15:0],regimm);
alucontrolunit alucon(aluop,instruction[5:0],alucontrol);
assign regb=(alusrc==1'b1)? regimm :regrr;
alu mainalu(rega,regb,alucontrol,aluout,zerosig);
data_memory dat(clk,aluout,regrr,memwrite,memread,memreaddata);
assign writedata=(memtoreg)? memreaddata:aluout;
Shift2bit sign(regimm,brjoffset);
assign beq=br&zerosig;
assign pcbreak=pcplus4+brjoffset;
assign pcj={pcplus4[31:28],brjoffset[27:0]};
assign next_pc= (beq==1'b1)? pcbreak :(j==1'b1)? pcj : pcplus4;
assign pcout =instruction;
endmodule










module mipstest;
reg rst,clk;
wire[31:0] aluout,pcout;
mips mipss(clk,rst,aluout,pcout);
always #1 clk=!clk;
initial 
begin 
rst=1'b1;
clk=1'b1;
$monitor("clock=%b, aluout=%d pcout=%b",clk,aluout,pcout);
 #1 rst = 1'b0;
#8 $finish;
end
endmodule


 